import 'dart:math';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/rendering.dart';

import 'scale.dart';
import 'label.dart';
import 'setpoint.dart';

const kTickTextStyle = TextStyle(fontSize: 10);
const kLabelTextStyle = TextStyle(fontSize: 12, fontWeight: FontWeight.bold);
const kTickLength = 8.0;
const kTickLabelSpace = 5.0;
const kLabelSpace = 10.0;
const kBarToOutline = 4.0;
const kMinBarTop = 15.0;
const kSetpointSize = 8.0;
const kSetpointColor = Color.fromRGBO(255, 0, 0, 1.0);

class ThermometerPainter extends CustomPainter {

  final double radius;
  final double barWidth;
  final double outlineThickness;
  final Color outlineColor;
  final Color mercuryColor;
  final Color backgroundColor;
  final double minValue;
  final double maxValue;
  final double value;
  final ScaleProvider scale;
  final bool mirrorScale;
  final ThermometerLabel label;
  final Setpoint setpoint;


  ThermometerPainter({
    @required this.radius,
    @required this.barWidth,
    @required this.outlineThickness,
    @required this.outlineColor,
    @required this.mercuryColor,
    @required this.backgroundColor,
    @required this.minValue,
    @required this.maxValue,
    @required this.value,
    @required this.scale,
    @required this.mirrorScale,
    @required this.label,
    @required this.setpoint
  });

  @override
  void paint(Canvas canvas, Size size) {

    final backgroudPaint = Paint() .. color = backgroundColor;
    final bgRect = Rect.fromLTWH(0, 0, size.width, size.height);
    canvas.drawRect(bgRect, backgroudPaint);
    canvas.clipRect(bgRect);

    final outlinePaint =
      Paint()
      ..color = outlineColor
      ..style = PaintingStyle.fill;

    final mercuryPaint =
      Paint()
      ..color = mercuryColor
      ..style = PaintingStyle.fill;

    // Calculated drawing parameters
    final center = Offset(size.width / 2, size.height - radius);
    final height = barWidth/2.0;
    final mercuryRadius = radius - outlineThickness - kBarToOutline;
    final minBarTop = center.dy - mercuryRadius - kMinBarTop;
    final maxBarTop = height;
    final valueToHeight = (v) => minBarTop + (maxBarTop - minBarTop)*(v - minValue)/(maxValue - minValue);
    final mercuryTop = valueToHeight(value);
    final mercuryHalfWidth = barWidth / 2.0 - outlineThickness - kBarToOutline;

    // Draw the outline of the thermometer
    final outer = _drawThermometerOutline(
      center: center, 
      radius: radius, 
      barHeight: height, 
      barWidth: barWidth,
      thickness: outlineThickness);

    canvas.drawPath(outer, outlinePaint);

    // Draw the mercury reservoir
    canvas.drawCircle(center, mercuryRadius, mercuryPaint);

    // Draw mercury bar
    canvas.drawRect(Rect.fromLTRB(
        center.dx - mercuryHalfWidth, 
        mercuryTop, 
        center.dx + mercuryHalfWidth, 
        center.dy),
      mercuryPaint);

    final applyOutlineColor = (TextStyle t) =>
      t.color == null? t.apply(color: outlineColor) : t;

    // Draw scale
    final tickMarkXBase = mirrorScale? center.dx + barWidth / 2 : center.dx - barWidth / 2;
    if (scale != null) {
      final ticks = scale.calcTicks(minValue, maxValue);
      ticks.forEach((tick) {
        final tickHeight = valueToHeight(tick.value);
        final tickThickness = (tick.thickness ?? outlineThickness)/2;

        // Draw the tick mark

        canvas.drawRect(
          Rect.fromLTRB(
            tickMarkXBase - (mirrorScale? -1 : 1)*(tick.length ?? kTickLength),
            tickHeight - tickThickness,
            tickMarkXBase,
            tickHeight + tickThickness),
          outlinePaint);


        if (tick.label != null) {
          final tickLabelSpan = TextSpan(
            style: applyOutlineColor(tick.textStyle ?? kTickTextStyle),
            text: tick.label
          );
          final tickLabelPainter = TextPainter(
            text: tickLabelSpan,
            textAlign: TextAlign.left,
            textDirection: TextDirection.ltr
          );
          tickLabelPainter.layout();
          if (mirrorScale) {
            tickLabelPainter.paint(canvas, Offset(
              tickMarkXBase + (tick.length ?? kTickLength) + (tick.labelSpace ?? kTickLabelSpace), 
              tickHeight - tickLabelPainter.height/2
              )
            );
          }
          else {
            tickLabelPainter.paint(canvas, Offset(
              tickMarkXBase - (tick.length ?? kTickLength) - (tick.labelSpace ?? kTickLabelSpace) - tickLabelPainter.width, 
              tickHeight - tickLabelPainter.height/2
              )
            );
          }
        }

      });
    }

    // Draw label
    if (label != null) {
      final labelTextSpan = TextSpan(
        text: label.label,
        style: applyOutlineColor(label.textStyle ?? kLabelTextStyle)
      );
      final labelPainter = TextPainter(
        text: labelTextSpan,
        textAlign: TextAlign.left,
        textDirection: TextDirection.ltr
      );
      labelPainter.layout();
      labelPainter.paint(canvas, 
        Offset(
          mirrorScale? 
            center.dx - barWidth/2 - kLabelSpace - labelPainter.width : 
            center.dx + barWidth/2 + kLabelSpace,
          0
        )
      );
    }

    // Draw setpoint
    if (setpoint != null) {

      final setpointSize = setpoint.size ?? kSetpointSize;
      final setpointY = valueToHeight(setpoint.value);
      final setpointPaint = Paint()
        ..color = setpoint.color ?? kSetpointColor
        ..style = PaintingStyle.fill;

      // Left
      if (setpoint.side == null || setpoint.side == SetpointSide.left || setpoint.side == SetpointSide.both) {
        final leftX = center.dx - barWidth/2;// - outlineThickness/2;
        final leftSetpointMark = Path()
          ..moveTo(leftX, setpointY)
          ..relativeLineTo(-setpointSize, setpointSize/2)
          ..relativeLineTo(0, -setpointSize)
          ..close();
        canvas.drawPath(leftSetpointMark, setpointPaint);
      }

      // Right
      if (setpoint.side == null || setpoint.side == SetpointSide.right || setpoint.side == SetpointSide.both) {
        final rightX = center.dx + barWidth/2;
        final rightSetpointMark = Path()
          ..moveTo(rightX, setpointY)
          ..relativeLineTo(setpointSize, setpointSize/2)
          ..relativeLineTo(0, -setpointSize)
          ..close();
          canvas.drawPath(rightSetpointMark, setpointPaint);
      }

    }


  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }


  Path _drawThermometerOutline({
    @required Offset center, 
    @required double radius, 
    @required double barHeight, 
    @required double barWidth,
    @required double thickness,
    }) {
    
    final centerX = center.dx;
    final centerY = center.dy;

    final dxOuter = barWidth / 2.0;
    final dyOuter = sqrt(radius*radius - (barWidth/2.0)*(barWidth/2.0));

    final innerRadius = radius - thickness;
    final innerWidth = barWidth - 2*thickness;
    final dxInner = innerWidth / 2.0;
    final dyInner = sqrt(innerRadius*innerRadius - (innerWidth/2.0)*(innerWidth/2.0));

    return Path()
      // Outer line
      ..moveTo(centerX - dxOuter, centerY - dyOuter)
      ..arcToPoint(Offset(centerX + dxOuter, centerY - dyOuter), radius: Radius.circular(radius), largeArc: true, clockwise: false)
      ..lineTo(centerX + dxOuter, barHeight)
      ..arcToPoint(Offset(centerX - dxOuter, barHeight), radius: Radius.circular(barWidth/2.0), clockwise: false)
      ..lineTo(centerX - dxOuter, centerY - dyOuter)
      ..close()

      // Inner line
      ..moveTo(centerX + dxInner, centerY - dyInner)
      ..arcToPoint(Offset(centerX - dxInner, centerY - dyInner), 
        radius: Radius.circular(innerRadius), largeArc: true)
      ..lineTo(centerX - dxInner, barHeight)
      ..arcToPoint(Offset(centerX + dxInner, barHeight),
        radius: Radius.circular(innerWidth/2.0))
      ..close()
      ;
  }

}
