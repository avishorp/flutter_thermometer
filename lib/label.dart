import 'package:flutter/rendering.dart';

/// Thermometer label
/// 
/// The thermometer label is a short text string, drawn near
/// the top of the widget, on the side opposite to the scale. It
/// is usually used to denote the temperature units.
class ThermometerLabel {

  /// The text to show on the label.
  final String label;

  /// The text style to be used to draw the label.
  final TextStyle textStyle;

  /// Create a [ThermometerLabel] instance.
  /// 
  /// A label text is mandatory and must not be null. If no
  /// [textStyle] is provided, default style will be used. 
  ThermometerLabel(this.label, { this.textStyle }):
    assert(label != null);

  /// Creates a label showing "°C", with default styling.
  static celsius() => ThermometerLabel('\u{00b0}C');

  /// Creates a label showing "°F", with default styling.
  static farenheit() => ThermometerLabel('\u{00b0}F');

  /// Creates a new copy of the object, altering the specified properties.
  ThermometerLabel apply({
    String label,
    TextStyle textStyle
  }) => ThermometerLabel(
    label ?? this.label,
    textStyle: textStyle ?? this.textStyle
  );
}

