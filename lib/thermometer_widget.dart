
import 'package:flutter/widgets.dart';

import 'thermometer_paint.dart';
import 'label.dart';
import 'scale.dart';
import 'setpoint.dart';

/// A Thermometer Widget
/// 
/// The [Thermometer] widget displays a themometer that shows a temperature
/// defiled by its [value] property. The properties [minValue] and [maxValue]
/// designate the minimum and the maximum temperature values that can be shown.
/// The widget can have optional features such as scale, label and setpoint.
/// 
/// ![Thermometer Anatomy](https://gitlab.com/avishorp/flutter_thermometer/-/raw/67d2e72e813c3411744a79792f86dd4065674df0/images/anatomy.gif?inline=false)
/// 
/// The properties [radius], [barWidth] and [outlineThickness] can be used to
/// alter the way the thermometer geometry looks. If not provided, default values
/// will be used. The widget will fill up the entire height it is given. The width
/// of the widget must be sufficient to hold all its features.
/// 
/// The properties [outlineColor], [mercuryColor] and [backgroundColor] control the colors
/// used for drawing the thermometer. They default to black, red and transparent respectively.
/// 
/// The [scale] property can be set to provide a temperature scale. The scale is defined by
/// an instance implementing the [ScaleProvider] interface. The package comes with a single
/// implementation - see [IntervalScaleProvider] - but custom implementations can be used
/// to create customized scales. If not provided, not scale will be drawn. The default location
/// of the scale is on the left side of the thermometer. To draw it on the right side, set
/// the [mirrorScale] property to true.
/// 
/// The [label] property draws a label, usually used to designate the temperature units shown
/// by the thermometer. It should be set to an instance of [ThermometerLabel], which allows the
/// customization of the text and its style. For convenience, two stock labels are provided -
/// [ThermometerLabel.celsius()] and [ThermometerLabel.farenheit()].
/// 
/// The [setpoint] property draws a small triangle of the scale of the thermometer (or on the
/// other side or both), usually used to denote a temperature target. This property can be assigned
/// with a [Setpoint] instance, which also include some styling options.
class Thermometer extends StatelessWidget {
  
  /// The radius used to draw the Mercury reservoir element.
  final double radius;

  /// The outer width of the thermometer bar.
  final double barWidth;

  /// The thickness of the thermometer outline.
  final double outlineThickness;

  /// The color used to draw the thermometer outline.
  final Color outlineColor;

  /// The color used to draw the mercury inside the thermometer.
  final Color mercuryColor;

  /// The color used to draw the background.
  final Color backgroundColor;

  /// Minimum temperature.
  final double minValue;

  /// Maximum temperature.
  final double maxValue;

  /// The current temperature shown.
  final double value;

  /// An object describing how to draw the scale. If null, no scale is drawn.
  final ScaleProvider scale;

  /// Draw the scale on the right side instead of the left side.
  final bool mirrorScale;

  /// A temperature unit label. If null, no label is drawn.
  final ThermometerLabel label;

  /// A temperature setpoint. If null, no setpoint is drawn.
  final Setpoint setpoint;

  /// Create a Thermometer instance
  /// 
  /// [value], [minValue] and [maxValue] are required. All other parameters are optional.
  /// The [value] property must be between [minValue] and [maxValue].
  Thermometer({
    Key key,
    @required this.minValue,
    @required this.maxValue,
    @required this.value,
    this.radius = 30.0,
    this.barWidth = 30.0,
    this.outlineThickness = 5.0,
    this.outlineColor = const Color.fromRGBO(0, 0, 0, 1.0),
    this.mercuryColor = const Color.fromRGBO(255, 0, 0, 1.0),
    this.backgroundColor = const Color.fromRGBO(0, 0, 0, 0.0),
    this.scale,
    this.mirrorScale = false,
    this.label,
    this.setpoint
  }):
  assert(maxValue > minValue, 'Maximal value must be greater than minimal value'),
  assert(value >= minValue && value <= maxValue, 'value must be between minimum and maximum'),
  assert(radius >= barWidth, 'Radius must be greater than bar width'),
  assert(barWidth > outlineThickness + 10, 'Bar width must be greater than outline thickness'),
  assert(outlineThickness > 0, 'Outline thickness must be positive'),
  assert(outlineColor != null),
  assert(mercuryColor != null),
  assert(backgroundColor != null),
  assert(setpoint == null || (setpoint.value >= minValue && setpoint.value <= maxValue),
    'Setpoint value must be between minimum and maximum'),
  super(key: key);


  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return CustomPaint(
        size: Size(constraints.maxWidth, constraints.maxHeight),
        painter: ThermometerPainter(
          radius: radius,
          barWidth: barWidth,
          outlineThickness: outlineThickness,
          outlineColor: outlineColor,
          mercuryColor: mercuryColor,
          backgroundColor: backgroundColor,
          minValue: minValue,
          maxValue: maxValue,
          value: value,
          scale: scale,
          mirrorScale: mirrorScale,
          label: label,
          setpoint: setpoint
          )
        );
    }
 );  
  }
}
