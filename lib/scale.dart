import 'package:flutter/rendering.dart';

/// Descibes a single scale tick.
/// 
/// Holds all the properties requried to draw a single scale tick mark.
class ScaleTick {
  /// The value (temperature) of the tick.
  final double value;

  /// The length of the tick (perpendicular to the outline), in pixels.
  final double length;

  /// The thickness of the tick, in pixels. If not defined, the thickness
  /// defaults to the outline thickness.
  final double thickness;

  /// The text label attached to the tick.
  final String label;

  /// The space, in pixels between the tick mark and the label.
  final double labelSpace;

  /// The text style used for drawing the tick label. If the color in the label
  /// is not defined, it defaults to the outline color.
  final TextStyle textStyle;

  /// Create a scale tick.
  /// 
  /// [value] is mandatory and must not be null.
  ScaleTick(this.value, { 
    this.label, 
    this.length, 
    this.thickness, 
    this.textStyle,
    this.labelSpace
    }):
    assert(value != null);
}

/// Interface for a class providing scale ticks.
/// 
/// A class implementing this interface calculates a list of scale
/// ticks to draw on the thermometer. Each tick can be individually tuned for
/// various properties such as color, style and label. 
abstract class ScaleProvider {

  /// Calculate the list of ticks to display.
  /// 
  /// For a given range (minValue to maxValue), calculates a list
  /// of scale ticks to be displayed.
  List<ScaleTick> calcTicks(double minValue, double maxValue);
}

/// Provides scale ticks at constant intervals.
class IntervalScaleProvider extends ScaleProvider {

  /// The temperature interval on which to draw the tick marks.
  final double interval;

  /// Number of fraction digits to show on the each number.
  final int fractionDigits;

  /// The length of the all the tick mark.
  final double length;

  /// The thickness of all the tick marks.
  final double thickness;

  /// Creates an interval scale provider.
  /// 
  /// The [interval] must be non-null and greater than zero. All other parameters are
  /// optional, defaults will be used if not specified.
  IntervalScaleProvider(this.interval, { this.fractionDigits = 1, this.length, this.thickness }):
    assert(interval > 0),
    assert(fractionDigits >= 0),
    assert(length == null || length > 0),
    assert(thickness == null || thickness > 0);

  @override
  List<ScaleTick> calcTicks(double minValue, double maxValue) {
    double v = minValue;
    List<ScaleTick> ticks = [];

    while(v <= maxValue) {
      ticks.add(ScaleTick(v, 
        label: v.toStringAsFixed(fractionDigits),
        length: length, 
        thickness: thickness));
      v += interval;
    }

    return ticks;
  }

  /// Creates a new copy of the object, altering the specified properties.
  IntervalScaleProvider apply(
    double interval,
    int fractionDigits,
    double length,
    double thickness
  ) => IntervalScaleProvider(
    interval ?? this.interval,
    fractionDigits: fractionDigits ?? this.fractionDigits,
    length: length ?? this.length,
    thickness: thickness ?? this.thickness
  );

}